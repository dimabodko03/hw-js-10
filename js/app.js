const tabs = document.querySelectorAll('.tabs-title');
const tabContent = document.querySelectorAll('.tabs-content li');

tabContent.forEach(content => {
    content.style.display = 'none';
});

tabs.forEach((tab, i) => {
    tab.addEventListener('click', () => {
        tabContent.forEach(content => {
            content.style.display = 'none';
            content.style.position = 'fixed';
            content.style.top = '90px';
            content.style.left = '20px';
        });

        tabContent[i].style.display = 'block';

        tabs.forEach(t => t.classList.remove('active'));
        tab.classList.add('active');
    });
});
